"""
Cubito
"""
from itertools import count
from time import sleep

import arcade
import cubito

# Screen title and size
SCREEN_TITLE = "Cubito"
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

# Margin between mat & screen side
VERTICAL_MARGIN_PERCENT = 0.10
HORIZONTAL_MARGIN_PERCENT = 0.10

# Token size
TOKEN_SCALE = 0.5
TOKEN_HEIGHT = 75 * TOKEN_SCALE
TOKEN_WIDTH = 75 * TOKEN_SCALE

# Space between tokens
X_SPACING_TOKEN = TOKEN_WIDTH + TOKEN_WIDTH * HORIZONTAL_MARGIN_PERCENT
Y_SPACING_TOKEN = TOKEN_HEIGHT + TOKEN_HEIGHT * VERTICAL_MARGIN_PERCENT

# List of token types
TOKEN_TYPES = ["FORDWARD", "RIGHT", "LEFT", "PAUSE", "FUNCTION"]

# Mat size
MAT_PERCENT_OVERSIZE = 1.25
MAT_HEIGHT = int(TOKEN_HEIGHT * MAT_PERCENT_OVERSIZE)
MAT_WIDTH = int(TOKEN_WIDTH * MAT_PERCENT_OVERSIZE)

# Number of column & row mat
MAT_COLUMN = 4
MAT_ROW = 4
MAT_FUNCTION_ROW = 2

# Space between mats
X_SPACING_MAT = MAT_WIDTH + MAT_WIDTH * HORIZONTAL_MARGIN_PERCENT
Y_SPACING_MAT = MAT_HEIGHT + MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Top for mats
TOP_Y = SCREEN_HEIGHT - MAT_HEIGHT - MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Bottom for tokens
BOTTOM_Y = MAT_HEIGHT / 2 + MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Start from left side
START_X = MAT_WIDTH / 2 + MAT_WIDTH * HORIZONTAL_MARGIN_PERCENT


class Token(arcade.Sprite):
    """Token sprite"""

    # Sprite from token type
    sprite = {
        "FORDWARD": "up",
        "RIGHT": "r",
        "LEFT": "l",
        "PAUSE": "pause_square",
        "FUNCTION": "hamburger",
    }

    def __init__(self, token_type, scale=1):
        """Token constructor"""
        # Type of token
        self.type = token_type
        # Get sprite for type of token
        self.image_file_name = (
            ":resources:onscreen_controls/shaded_dark/%s.png"
            % self.sprite.get(token_type, "unchecked")
        )

        # Init parent class
        super().__init__(self.image_file_name, scale, hit_box_algorithm="None")


class Mat(arcade.SpriteSolidColor):
    """Mat sprite"""

    # Count instance mat classes
    ids = count(0)

    def __init__(self, wigth, height, color=arcade.csscolor.DARK_OLIVE_GREEN):
        # Get id from number of instance classes
        self.id = next(self.ids)

        # Init parent class
        super().__init__(wigth, height, color)


class Cubito(arcade.Window):
    """Main application class"""

    def __init__(self):
        # Init parent class
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
        # Set background color
        arcade.set_background_color(arcade.color.AMAZON)

        # List of tokens
        self.token_list = None

        # Hold token
        self.held_token = None

        # Origin pos for hold token
        self.held_token_original_position = None

        # List of mats
        self.mat_list = None

        # List of mats function
        self.mat_function_list = None

    def setup(self):
        """Set up the game"""
        # List of cards we are dragging with the mouse
        self.held_token = None

        self.held_token_original_position = None

        self.mat_list = arcade.SpriteList()

        for j in range(MAT_ROW):
            # Create the top "play" piles
            for i in range(MAT_COLUMN):
                mat = Mat(MAT_WIDTH, MAT_HEIGHT)
                mat.position = (
                    START_X + i * X_SPACING_MAT + SCREEN_WIDTH / 2,
                    TOP_Y - Y_SPACING_MAT * j,
                )
                self.mat_list.append(mat)

        self.mat_function_list = arcade.SpriteList()

        for j in range(MAT_ROW, MAT_ROW + MAT_FUNCTION_ROW):
            # Create the top "play" piles
            for i in range(MAT_COLUMN):
                mat = Mat(MAT_WIDTH, MAT_HEIGHT, arcade.color.COOL_BLACK)
                mat.position = (
                    START_X + i * X_SPACING_MAT + SCREEN_WIDTH / 2,
                    TOP_Y - Y_SPACING_MAT * j,
                )
                self.mat_function_list.append(mat)

        # Sprite list with all the cards, no matter what pile they are in.
        self.token_list = arcade.SpriteList()

        # Create every token
        for j, t in enumerate(TOKEN_TYPES):
            for i in range(10):
                token = Token(t, TOKEN_SCALE)
                token.position = (
                    START_X + X_SPACING_TOKEN * j,
                    BOTTOM_Y + X_SPACING_TOKEN * i,
                )
                self.token_list.append(token)
                i += 50

    def on_draw(self):
        """Render the screen"""
        # Clear the screen
        arcade.start_render()

        # Draw the mats
        self.mat_list.draw()

        # Draw the mats function
        self.mat_function_list.draw()

        # Draw the tokens
        self.token_list.draw()

        # Draw token id in bottom left mat side
        all_mat = list(self.mat_list) + list(self.mat_function_list)
        for mat in all_mat:
            size = 10
            arcade.draw_text(
                mat.id,
                mat.left + size / 3,
                mat.bottom + size / 3,
                arcade.color.CHARTREUSE,
                size,
            )

    def on_mouse_press(self, x, y, button, key_modifiers):
        """Called when the user presses a mouse button"""
        # Get list of token when click
        tokens = arcade.get_sprites_at_point((x, y), self.token_list)

        # Check if grab a token
        if len(tokens) > 0:
            # Hold the token
            self.held_token = tokens[-1]
            # Set original pos for this token
            self.held_token_original_position = self.held_token.position

    def on_mouse_release(self, x, y, button, modifiers):
        """Called when the user presses a mouse button"""
        # If no token hold, pass
        if self.held_token is None:
            return

        # Find the closert mat from held token
        mat, distance = arcade.get_closest_sprite(self.held_token, self.mat_list)
        reset_position = True

        # Check collision between held token et the closert mat
        if arcade.check_for_collision(self.held_token, mat):
            # Set held token position on mat
            self.held_token.position = mat.center_x, mat.center_y

            reset_position = False

        # Find the closert mat function from held token
        mat, distance = arcade.get_closest_sprite(
            self.held_token, self.mat_function_list
        )

        # Check collision between held token et the closert mat
        if arcade.check_for_collision(self.held_token, mat):
            # Set held token position on mat
            self.held_token.position = mat.center_x, mat.center_y

            reset_position = False

        # If no collision between held token et the closert mat, reset held token position
        if reset_position:
            self.held_token.position = self.held_token_original_position

        # Release hold token
        self.held_token = None

    def on_mouse_motion(self, x, y, dx, dy):
        """Called when the user moves the mouse"""
        # If no token hold, pass
        if self.held_token is None:
            return

        self.held_token.center_x += dx
        self.held_token.center_y += dy

    def on_key_press(self, symbol, modifiers):
        """Called when the user presses key"""

        # Press « R » key
        if symbol == arcade.key.R:
            print("Restart !")
            # Reset mat ids
            Mat.ids = count(0)
            # Relunch setup methode
            self.setup()

        # Press « S » key
        if symbol == arcade.key.S:
            # Run cubito
            self.cubito()

        # Press « Q » key
        if symbol == arcade.key.Q:
            print("Good bye !")
            # Exit
            arcade.exit()

    def cubito(self, function=False):
        """Move cubito !"""
        # Get list of mats
        if function:
            mats = self.mat_function_list
        else:
            mats = self.mat_list

        for mat in mats:
            # Find the closert token from mat
            token, distance = arcade.get_closest_sprite(mat, self.token_list)
            # Check collision between the closert token & mat
            if arcade.check_for_collision(mat, token):
                msg = token.type
                print("> Move %s : %s" % (mat.id, msg))
                if msg == "FORDWARD":
                    cubito.fordward()
                elif msg == "LEFT":
                    cubito.left()
                elif msg == "RIGHT":
                    cubito.right()
                elif msg == "FUNCTION" and not function:
                    self.cubito(function=True)
                elif msg == "PAUSE":
                    pass
                else:
                    print("Unknow command")
            else:
                print("> Move %s :" % mat.id)
            sleep(0.1)


def main():
    """Main method"""
    window = Cubito()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
